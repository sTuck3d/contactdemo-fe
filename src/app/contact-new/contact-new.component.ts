import {Component, Input, OnInit} from '@angular/core';
import {Contact} from '../shared/contact';
import {RestApiService} from '../shared/rest-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-contact-new',
  templateUrl: './contact-new.component.html',
  styleUrls: ['./contact-new.component.css']
})
export class ContactNewComponent implements OnInit {

  firstName: string;
  lastName: string;
  address: string;

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  addContact() {
    const contactData = new Contact(this.address, this.firstName, this.lastName);
    this.restApi.addContact(contactData);
  }
}
