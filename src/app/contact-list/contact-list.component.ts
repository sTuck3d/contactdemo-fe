import { Component, OnInit } from '@angular/core';
import {RestApiService} from '../shared/rest-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  Contacts: any = [];

  constructor(
    public restApi: RestApiService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.loadContacts();
  }

  loadContacts() {
    return this.restApi.getAllContacts().subscribe((data: {}) => {
      this.Contacts = data;
    });
  }

  gotoEditContact(address) {
    console.log('address: ' + address);
    this.router.navigate(['contact/edit/' + address]);
  }

  deleteContact(address) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.restApi.deleteContact(address).subscribe(data => {
        this.loadContacts();
      });
    }
  }

}
