import { Component, OnInit } from '@angular/core';
import {RestApiService} from '../shared/rest-api.service';
import {Contact} from '../shared/contact';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {

  address: string;
  lastName: string;
  firstName: string;

  constructor(
    public restApi: RestApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadContact(this.route.snapshot.params.address);
  }

  loadContact(address) {
    this.restApi.getContact(address).subscribe(contact => {
      this.address = contact.address;
      this.firstName = contact.firstName;
      this.lastName = contact.lastName;
    });
  }

  saveContact() {
    const contact = new Contact(this.address, this.lastName, this.firstName);
    this.restApi.saveContact(contact).subscribe(data => {});
  }

}
