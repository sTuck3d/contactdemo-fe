import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContactListComponent} from './contact-list/contact-list.component';
import {ContactEditComponent} from './contact-edit/contact-edit.component';
import {ContactNewComponent} from './contact-new/contact-new.component';

const routes: Routes = [
  { path: '', redirectTo: '/contacts', pathMatch: 'full' },
  {path: 'contacts' , component: ContactListComponent},
  {path: 'contact/edit/:address' , component: ContactEditComponent},
  {path: 'contact/new' , component: ContactNewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
