export class Contact {
  address: string;
  lastName: string;
  firstName: string;

  constructor(adress: string, firstName: string, lastName: string) {
    this.address = adress;
    this.lastName = lastName;
    this.firstName = firstName;
  }

}
