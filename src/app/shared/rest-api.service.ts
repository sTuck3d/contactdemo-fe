import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Contact} from './contact';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURL = 'http://localhost:8181';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getAllContacts(): Observable<Contact> {
    return this.http.get<Contact>(this.apiURL + '/contacts', this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  saveContact(contact): Observable<Contact> {
    console.log('saveContact: ' + contact.address + ',' + contact.firstName + ',' + contact.lastName );
    return this.http.put<Contact>(this.apiURL + '/contact/change', contact, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getContact(address): Observable<Contact> {
    return this.http.get<Contact>(this.apiURL + '/contact/' + address, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  deleteContact(address) {
    return this.http.put<Contact>(this.apiURL + '/contact/delete/' + address, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  purgeData() {
    return this.http.post(this.apiURL + '/contacts/purge' + '', this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  addContact(contact: Contact) {
    console.log('writing contact: ' + contact.address);
    return this.http.post(this.apiURL + '/contact/new', contact, this.httpOptions)
      .subscribe(
        (val) => {
          console.log('POST call successful value returned in body',
            val);
        },
        response => {
          this.handleError(response);
        },
        () => {
          console.log('The POST observable is now completed.');
        });
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
