import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { HttpClientModule } from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import { FooterButtonsComponent } from './footer-buttons/footer-buttons.component';
import { AppRoutingModule } from './app-routing.module';
import { ContactNewComponent } from './contact-new/contact-new.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const appRoutes: Routes = [
  { path: 'contact-list', component: ContactListComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ContactEditComponent,
    ContactListComponent,
    FooterButtonsComponent,
    ContactNewComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
