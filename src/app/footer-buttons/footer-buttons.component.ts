import { Component, OnInit } from '@angular/core';
import {RestApiService} from '../shared/rest-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-footer-buttons',
  templateUrl: './footer-buttons.component.html',
  styleUrls: ['./footer-buttons.component.css']
})
export class FooterButtonsComponent implements OnInit {

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  newContact() {
    this.router.navigate(['/contact/new']);
  }

  purgeData() {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.restApi.purgeData().subscribe(data => {
        this.router.navigate(['/contacts']);
      });
    }
  }



}
